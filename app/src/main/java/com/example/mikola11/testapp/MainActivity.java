package com.example.mikola11.testapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }

    public void btn_click_close(View v){
        finish();
    }
    public void btn_click_toast(View v){
        Toast mytoast = Toast.makeText(this, "Something text", Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        mytoast.show();
    }
}
